<?php
//:D
require_once 'iDB.interaface.php';


class DB implements iDB
{
    private $connection;
    private $handler;
    private $data;

    public function __construct($host, $login, $password, $dbName)
    {
        $dsn =  'mysql:host=' . $host.
            ';dbname=' . $dbName;


        try {
            $this->connection = new PDO($dsn, $login, $password);
        } catch (PDOException $e) {
            throw new Exception('Cannot connect');
        }
    }

    public function query($query)
    {
        if ($this->handler)
            $this->handler->closeCursor();
        try {
            $this->handler = $this->connection->query($query);
        } catch (PDOException $e) {
            throw new Exception('Query failed');
        }
    }

    public function getAffectedRows()
    {
        return $this->handler->rowCount();
    }

    public function getRow()
    {
        return $this->handler->fetch();
    }

    public function getAllRows()
    {
        return $this->handler->fetchAll(PDO::FETCH_ASSOC);
    }
}
