<?php

abstract class BaseProduct
{
    private $fields;

    /**
     * BaseProduct constructor.
     */
    function __construct()
    {
    }

    /**
     * @param $name
     * @param $arguments
     *
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        $field = substr($name, 3, strlen($name));
        $field = lcfirst($field);
        $type = substr($name, 0,3);

        if ($type === 'set') {
            $this->fields[$field] = $arguments[0];
        } else if ($type === 'get') {
            if(array_key_exists($field, $this->fields))
                return $this->fields[$field];
        }
    }
}