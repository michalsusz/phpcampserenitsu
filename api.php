<?php

require_once 'db.php';

function array_to_xml($array, &$xml) {
    foreach($array as $key => $value) {
        if(is_array($value)) {
            if(!is_numeric($key)){
                $subnode = $xml->addChild("$key");
                array_to_xml($value, $subnode);
            } else {
                array_to_xml($value, $xml);
            }
        } else {
            $xml->addChild("$key","$value");
        }
    }
}


function api()
{
    try {

        $db = new DB('172.17.0.2', 'root', '111111', 'admin');

        if (!isset($_GET['action']) || !array_key_exists('action', $_GET)) {
            echo json_encode(['msg' => 'No action set!']);
            return;
        }

        if (!is_string($_GET['action'])) {
            echo json_encode(['msg' => 'Incorrect action parameter!']);
            return;
        }

        switch ($_GET['action'])
        {
            case 'check':
                $sql = '';
                if (isset($_GET['id'])) {
                    $id = filter_var($_GET['id'], FILTER_VALIDATE_INT);
                    $id = (int)$id;
                    $sql = 'SELECT id, name, price FROM products WHERE id=\'' . $id . '\'';
                } else {
                    $sql = 'SELECT id, name, price FROM products';
                }

                $db->query($sql);
                if(isset($_GET['xml'])) {

                } else {
                    echo json_encode($db->getAllRows());
                }
                return;
                break;
            case 'add':
                $name = isset($_GET['name']) ? $_GET['name'] : 'unknown';
                $name = filter_var($name, FILTER_SANITIZE_STRING);
                $price = isset($_GET['price']) ? $_GET['price'] : '0';
                $price = filter_var($price, FILTER_SANITIZE_NUMBER_FLOAT);
                $sql = "INSERT INTO products(name, price) VALUES('".$name."', '".$price."')";
                $db->query($sql);
                return;
                break;
            case 'remove':
                $id = filter_var($_GET['id'], FILTER_VALIDATE_INT);
                $id = (int)$id;
                $sql = "DELETE FROM products WHERE id='" . $id . "'";
                $db->query($sql);
                break;
        }


    } catch (Exception $e) {
        echo $e->getMessage();
    }

}

api();