<?php

class DB implements iDB
{
    private $connection;
    private $handler;
    private $data;

    public function __construct($host, $login, $password, $dbName)
    {
        $dsn =  'mysql:host=' . $host.
            ';dbname=' . $dbName;


        try {
            $this->connection = new PDO($dsn, $login, $password);
        } catch (PDOException $e) {
            throw new Exception('Cannot connect');
        }
    }

    public function query($query)
    {
        if ($this->handler)
            $this->handler->closeCursor();
        try {
            $this->handler = $this->connection->query($query);
        } catch (PDOException $e) {
            throw new Exception('Query failed');
        }
    }

    public function getAffectedRows()
    {
        return $this->handler->rowCount();
    }

    public function getRow()
    {
        return $this->handler->fetch();
    }

    public function getAllRows()
    {
        return $this->handler->fetchAll(PDO::FETCH_ASSOC);
    }
}

class Service
{

    private $dbase;
    private $wasDBError;

    public function __construct()
    {
        try {
            $this->dbase = new DB('172.17.0.2', 'root', '111111', 'admin');
        } catch (Exception $e) {
            $this->wasDBError = $e->getCode() . ': ' . $e->getMessage();
        }
    }

    private function sendData($data) {
        header('Content-type: text/json');
        if (strlen($this->wasDBError)) {
            echo json_encode(['msg' => $this->wasDBError]);
            return;
        }

        echo json_encode($data);
    }

    public function checkData($id)
    {
        if (!strlen($this->wasDBError)) {
            $this->sendData(NULL);
            return;
        }

        $sql = '';

        if (!is_null($id)) {
            $id = filter_var($id, FILTER_VALIDATE_INT);
            $id = (int)$id;
            $sql = 'SELECT id, name, price FROM products WHERE id=\'' . $id . '\'';
        } else {

            $sql = 'SELECT id, name, price FROM products';
        }
        try {
            $this->dbase->query($sql);
        } catch (Exception $e) {
            $this->wasDBError = $e->getCode() . ': ' . $e->getMessage();
        }

        $this->sendData($this->dbase->getAllRows());
    }

    public function removeData()
    {

    }

    public function addData()
    {

    }
}

$options = ['uri'   =>  '127.0.0.1:2001'];
$server = new SoapServer(NULL, $options);
$server->setClass('Service');
$server->handle();